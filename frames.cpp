#include "frames.h"

vec3f Frame::get_s(){
    vec3f s = crossProduct(up, forward);
    return vecNormalize(s);
}

Matrix Frame::toMatrix(){
    Matrix m;

    vec3f s = get_s();

    m.matrix[0] = s.x;
    m.matrix[1] = s.y;
    m.matrix[2] = s.z;
    m.matrix[3] = 0;

    m.matrix[4] = up.x;
    m.matrix[5] = up.y;
    m.matrix[6] = up.z;
    m.matrix[7] = 0;

    m.matrix[8] = forward.x;
    m.matrix[9] = forward.y;
    m.matrix[10] = forward.z;
    m.matrix[11] = 0;

    m.matrix[12] = pos.x;
    m.matrix[13] = pos.y;
    m.matrix[14] = pos.z;
    m.matrix[15] = 1;

    return m;
}

Matrix Frame::toCameraMatrix(){
    Matrix m;
    m.identityMatrix3f();

    forward = vecNormalize(forward);
    up = vecNormalize(up);

    vec3f s = crossProduct(forward, up);
    vec3f upp = crossProduct(s, forward);

    GLfloat V[16];
    GLfloat T[16];

    V[0] = s.x;
    V[4] = s.y;
    V[8] = s.z;
    V[12] = 0;

    V[1] = upp.x;
    V[5] = upp.y;
    V[9] = upp.z;
    V[13] = 0;

    V[2] = -forward.x;
    V[6] = -forward.y;
    V[10] = -forward.z;
    V[14] = 0;

    V[3] = 0;
    V[7] = 0;
    V[11] = 0;
    V[15] = 1;

    T[0] = 1;
    T[1] = 0;
    T[2] = 0;
    T[3] = 0;

    T[4] = 0;
    T[5] = 1;
    T[6] = 0;
    T[7] = 0;

    T[8] = 0;
    T[9] = 0;
    T[10] = 1;
    T[11] = 0;

    T[12] = -pos.x;
    T[13] = -pos.y;
    T[14] = -pos.z;
    T[15] = 1;

    m.multiply(V);
    m.multiply(T);

    return m;
}

void Frame::mulForward(GLfloat *matrix){
    int w = 1;
    vec3f fcopy = forward;
    forward.x = matrix[0] * fcopy.x + matrix[4] * fcopy.y + matrix[8] * fcopy.z + matrix[12] * w;
    forward.y = matrix[1] * fcopy.x + matrix[5] * fcopy.y + matrix[9] * fcopy.z + matrix[13] * w;
    forward.z = matrix[2] * fcopy.x + matrix[6] * fcopy.y + matrix[10] * fcopy.z + matrix[14] * w;

    forward = vecNormalize(forward);
}

void Frame::mulUp(GLfloat *matrix){
    int w = 1;
    vec3f ucopy = up;
    up.x = matrix[0] * ucopy.x + matrix[4] * ucopy.y + matrix[8] * ucopy.z + matrix[12] * w;
    up.y = matrix[1] * ucopy.x + matrix[5] * ucopy.y + matrix[9] * ucopy.z + matrix[13] * w;
    up.z = matrix[2] * ucopy.x + matrix[6] * ucopy.y + matrix[10] * ucopy.z + matrix[14] * w;

    up = vecNormalize(up);
}

void Frame::mulPosition(GLfloat *matrix){
    int w = 1;
    vec3f pcopy = pos;
    pos.x = matrix[0] * pcopy.x + matrix[4] * pcopy.y + matrix[8] * pcopy.z + matrix[12] * w;
    pos.y = matrix[1] * pcopy.x + matrix[5] * pcopy.y + matrix[9] * pcopy.z + matrix[13] * w;
    pos.z = matrix[2] * pcopy.x + matrix[6] * pcopy.y + matrix[10] * pcopy.z + matrix[14] * w;

    pos = vecNormalize(pos);
}
