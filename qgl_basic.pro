SOURCES += \
    main.cpp \
    glwidget.cpp \
    utils.cpp \
    matrices.cpp \
    meshes.cpp \
    frames.cpp \
    material.cpp \
    light.cpp

HEADERS += \
    glwidget.h \
    utils.h \
    matrices.h \
    meshes.h \
    frames.h \
    material.h \
    light.h

QT += opengl widgets

CONFIG += debug

win32{
    LIBS += -lglu32
}

unix{
    LIBS += -lGLU
}
