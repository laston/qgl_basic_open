#ifndef MATRICES_H
#define MATRICES_H

#include "utils.h"

struct Matrix{
    GLfloat matrix[16];

    //domnażanie macierzy podanej w argumencie do instniejącej w strukturze
    void multiply(GLfloat *matrix2);


    /* Funkcje generujące macierze */

    //macierz jednostkowa
    void identityMatrix3f();

    //macierz skalowania
    void scaleMatrix3f(float sx, float sy, float sz);

    //macierz translacji
    void translateMatrix3f(float tx, float ty, float tz);

    //macierz obrotu względem osi x
    void rotatexMatrix3f(float alpha);

    //macierz obrotu względem osi y
    void rotateyMatrix3f(float alpha);

    //macierz obrotu względem osi z
    void rotatezMatrix3f(float alpha);

    //macierz obrotu względem wektora
    void rotateMatrix3f(vec3f v, float alpha);



    //zwraca wskaźnik na tablicę/macierz
    GLfloat *loadMatrix();

};

#endif // MATRICES_H
