#ifndef QGLIDGET_H
#define QGLIDGET_H

#include <QTimer>
#include <QTime>
#include <QGLWidget>
#include <GL/glu.h>

#include "utils.h"
#include "meshes.h"
#include "matrices.h"
#include "frames.h"
#include "material.h"
#include "light.h"

class GLWidget : public QGLWidget
{
    Q_OBJECT

    QTimer timer;   // wyzwalacz dla kolejnych klatek
    QTime time;     // pomiar czasu
    int fps;        // miara ilosci klatek na sekunde

public:
    explicit GLWidget(QGLWidget *parent = 0);
    
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent * e);
    void keyPressEvent(QKeyEvent *);

private:
    enum activeScene{
        SOLAR_SYSTEM,
        TERRAIN,
        OBJECT
    };

    activeScene active_scene;
    boolean linesActive;

    void setMaterial(Material &material);
    void setLight(GLenum cap, Light &light);

    void drawLines();
    void drawObject();
    void drawSolarSystem();
    void drawTerrain();

    float lastCursorHPos;
    float lastCursorVPos;

    float rdeg;
    float slonceAxisR;
    float ziemiaOrbitR;
    float ziemiaAxisR;
    float ksiezycOrbitR;
    float ksiezycAxisR;

    float speed;

    Mesh *m;

    Mesh *terrain;
    Mesh *light_source;

    Mesh *slonce;
    Mesh *ziemia;
    Mesh *ksiezyc;

    Matrix MVMatrix;
    Matrix rotateMatrixAxisS;
    Matrix translateMatrixZ;
    Matrix rotateMatrixOrbitZ;
    Matrix rotateMatrixAxisZ;
    Matrix rotateMatrixTiltZ;
    Matrix translateMatrixK;
    Matrix rotateMatrixOrbitK;
    Matrix rotateMatrixAxisK;
    Matrix scaleMatrix;

    Matrix rotateCameraH;
    Matrix rotateCameraV;

    Frame camera;

    Material material0;
    Light light0;

    int faces;
signals:
    
public slots:
    void redraw();
};

#endif // QGLIDGET_H
