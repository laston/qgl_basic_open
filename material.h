#ifndef MATERIAL_H
#define MATERIAL_H

#include "utils.h"

class Material{

public:
    void setAmbientColor(float red, float green, float blue, float alpha);
    void setDiffuseColor(float red, float green, float blue, float alpha);
    void setSpecularColor(float red, float green, float blue, float alpha);
    void setShininess(float shin);

    float* getAmbientColor();
    float* getDiffuseColor();
    float* getSpecularColor();
    float getShininess();

private:
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float shininess = 0;

};


#endif // MATERIAL_H
